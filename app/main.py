import os
from flask import Flask, request, jsonify, Response
from flask_restful import Resource, Api
import storage
from storage import ServerStorage, ProfileStorage
import json

app = Flask(__name__)
api = Api(app)


# id, profile_id, properties
class Server(Resource):
    def __init__(self):
        self.storage = ServerStorage()

    def get(self, server_id):
        result = self.storage.select(server_id)
        if result:
            return result

        return None, 404

    def post(self, server_id):
        server_dict = json.loads(request.data)
        self.storage.update(server_id, **server_dict)

    def put(self):
        self.storage.insert()

    def delete(self, server_id):
        self.storage.delete(server_id)

# id, text
class Profile(Resource):
    def __init__(self):
        self.storage = ProfileStorage()

    def get(self, profile_id):
        result = self.storage.select(profile_id)
        if result:
            return result[0]

        return None, 404

    def post(self, profile_id):
        self.storage.update(profile_id, request.data)

    def put(self):
        self.storage.insert()

    def delete(self, profile_id):
        self.storage.delete(profile_id)



api.add_resource(Server, '/servers/<server_id>')
api.add_resource(Profile, '/profiles/<profile_id>')

@app.route("/partial_server_update/<server_id>", methods=["POST"])
def partial_server_update(server_id):
    ServerStorage().partial_update(
        server_id,
        json.loads(request.data)
    )


@app.route("/servers/<server_id>/profile", methods=["GET"])
def get_server_profile(server_id):
    return Response(ServerStorage().get_profile(server_id), mimetype="text/plain")

@app.route("/servers/<server_id>/<prop>", methods=["GET"])
def get_server_property(server_id, prop):
    return jsonify(
        ServerStorage().get_property(
            server_id,
            prop
        )
    )

@app.route("/servers/search", methods=["GET"])
def search():
    search_args = list(request.args.keys())
    print(search_args)

    if search_args:
        key = search_args[0]
        value = request.args.get(key)
        return jsonify(ServerStorage().search(key, value))


if __name__ == "__main__":
    if not os.path.isfile("init.lock"):
        storage.init()
    with open("init.lock", "w") as f:
        pass

    app.run(host='0.0.0.0', debug=True, port=8080)

