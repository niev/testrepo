import postgresql
import json


def init():
    db = postgresql.open("pq://postgres:postgres@postgres/postgres")
    db.execute('DROP TABLE servers')
    db.execute('DROP TABLE profiles')

    db.execute('CREATE TABLE servers (id SERIAL PRIMARY KEY, profile_id INTEGER, properties jsonb)')
    db.execute('CREATE TABLE profiles (id SERIAL PRIMARY KEY, text TEXT not null)')

    server1 = {
        "id": 1,
        "profile_id": 5,
        "properties": {
            "manufacter": "Supermicro",
            "cpu_count": 2,
            "cpu_model": "E5-2620v4"
        }
    }

    server2 = {
        "id": 2,
        "profile_id": 5,
        "properties": {
            "manufacter": "Supermicro",
            "mac_addresses": [
                "11:22:33:44:55:66",
                "ab:cd:ef:01:23:45"
            ],
            "rack": "msk-std-1/05/02"
        }
    }

    req = db.prepare("INSERT INTO servers (id, profile_id, properties) VALUES ($1, $2, $3)")
    for server in (server1, server2):
        req(server["id"], server["profile_id"], json.dumps(server["properties"]))

    req = db.prepare("INSERT INTO profiles (id, text) VALUES ($1, $2)")
    req(5,
        """some large
        multiline
        string
        """
    )


class ServerStorage():
    def __init__(self):
        self.db = postgresql.open("pq://postgres:postgres@postgres/postgres")

    def select(self, server_id):
        req = self.db.prepare("select id, profile_id, properties from servers where id = $1")
        result = req(int(server_id))
        if len(result):
            return {
                "id": result[0][0],
                "profile_id": result[0][1],
                "properties": json.loads(result[0][2]),
            }

    def insert(self):
        req = self.db.prepare("INSERT INTO servers")
        req()

    def delete(self, server_id):
        req = self.db.prepare("delete from servers where id = $1")
        req(int(server_id))

    def update(self, server_id, *args, **xargs):
        if "profile_id" in xargs:
            req = self.db.prepare("update servers set profiel_id=$1 where id = $2")
            req(xargs["profile.id"], int(server_id))

        if "properties" in xargs:
            req = self.db.prepare("update servers set properties=$1 where id = $2")
            req(json.dumps(xargs["properties"]), int(server_id))

    def partial_update(self, server_id, properties):
        req = self.db.prepare("select * from servers where id = $1")
        server = req(int(server_id))[0]

        props = json.loads(server.properties)
        props.update(properties)

        req = self.db.prepare("update servers set properties=$1 where id = $2")
        req(json.dumps(props), int(server_id))

    def search(self, key, value):
        req = self.db.prepare("select * from servers where properties->> $1 = $2")
        return [{
            "id": req_id,
            "profile_id": req_profile_id,
            "properties": json.loads(req_properties)
        }
            for req_id, req_profile_id, req_properties in req(str(key), str(value))
        ]

    def get_property(self, server_id, prop):
        server = self.select(server_id)
        if server:
            return server["properties"].get(prop)

    def get_profile(self, server_id):
        req = self.db.prepare("select profiles.text from servers left join profiles on servers.profile_id = profiles.id where servers.id = $1")
        return req(int(server_id))[0][0]


class ProfileStorage():
    def __init__(self):
        self.db = postgresql.open("pq://postgres:postgres@postgres/postgres")

    def select(self, profile_id):
        req = self.db.prepare("select * from profiles where id = $1")
        result = req(int(profile_id))
        if len(result):
            return {
                "id": result[0][0],
                "text": result[0][1]
            }

    def insert(self):
        req = self.db.prepare("INSERT INTO profiles")
        req()

    def delete(self, profile_id):
        req = self.db.prepare("delete from profiles where id = $1")
        req(int(profile_id))

    def update(self, profile_id, *args, **xargs):
        if "text" in xargs:
            req = self.db.prepare("update profiles set text=$1 where id = $2")
            req(json.dumps(xargs["text"]), int(profile_id))

