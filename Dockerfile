FROM python:3

RUN pip install flask flask-restful py-postgresql


COPY ./app /app

EXPOSE 8080
CMD ["python3", "/app/main.py"]
