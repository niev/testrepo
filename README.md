# Run POSTGRES
docker run -d --name=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres postgres

# Run application
docker build -t test .

docker run --link postgres:postgres -p 8080:8080  test

# Do not use on production database